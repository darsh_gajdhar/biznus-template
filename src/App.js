import Header from './common/Header/Header';
import Footer from './common/Footer/Footer';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './module/Home/Home';
import About from './module/About/About';
import Shop from './module/Shop/Shop';
import Donate from './module/Donate/Donate';
import Contact from './module/Contact/Contact';

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/home' element={<Home />} />
          <Route path='/about' element={<About />} />
          <Route path='/shop' element={<Shop />} />
          <Route path='/donate' element={<Donate />} />
          <Route path='/contact' element={<Contact />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
