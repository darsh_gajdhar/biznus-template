import { styled } from "@mui/system";
import { Drawer } from "@mui/material";
import IconButton from "@mui/material/IconButton";

const DrawerComponentContainer = styled(Drawer, {
    name: 'DrawerComponentContainer'
})({
    '.MuiDrawer-paper': {
        width: '95%',
        backgroundColor: '#eb5757',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        'a': {
            textDecoration: 'none',
            color: 'white',
            marginBottom: '15px',
            fontSize: '24px'
        }
    }
}
);

export const ButtonIcon = styled(IconButton, {
    name: 'ButtonIcon'
})({
    color: 'white'
})

export default DrawerComponentContainer;