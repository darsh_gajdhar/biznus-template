import React from "react";
import { Link } from 'react-router-dom';
import DrawerComponentContainer, { ButtonIcon } from "./style";
import MenuIcon from "@mui/icons-material/Menu";

const Drawer = ({
  isOpenDrawer,
  handleDrawer
}) => (
  <>
    <DrawerComponentContainer
      anchor='right'
      open={isOpenDrawer}
      onClose={() => handleDrawer()}
    >
      <Link to='/home'>Home</Link>
      <Link to='/about'>About</Link>
      <Link to='/shop'>Shop</Link>
      <Link to='/donate'>Donate</Link>
      <Link to='/contact'>Contact</Link>
    </DrawerComponentContainer>
    <ButtonIcon
      onClick={() => handleDrawer()}
    >
      <MenuIcon />
    </ButtonIcon>
  </>
);


export default Drawer;