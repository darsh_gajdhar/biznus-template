import React, { useState } from 'react';
import Drawer from './Drawer';

const DrawerContainer = () => {
    const [isOpenDrawer, setIsOpenDrawer] = useState(false);

    const handleDrawer = () => {
        setIsOpenDrawer((pervState) => !pervState);
    }

    return (
        <Drawer
            isOpenDrawer={isOpenDrawer}
            handleDrawer={handleDrawer}
        />
    )
}

export default DrawerContainer;