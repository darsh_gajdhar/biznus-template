import { styled } from "@mui/system";
import { Box, Typography } from "@mui/material";

const SupportCardComponent = styled(Box, {
    name: 'SupportCardComponent'
})({
    width: '100%'
})

export const SupportCard = styled(Box, {
    name: 'SupportCard'
})({
    height: '450px',
    boxSizing: 'border-box',
});

export const SupportCardSquare = styled(Box, {
    name: 'SupportCardSquare'
})({
    height: '75%',
    marginBottom: '30px',
    borderRadius: '5px',
    backgroundColor: '#212121',
    position: 'relative'
});

export const SupportCardNumber = styled(Typography, {
    name: 'SupportCardNumber'
})({
    color: 'white',
    fontWeight: 'bold',
    top: '40px',
    left: '30px',
    position: 'absolute',
});

export const SupportCardText = styled(Typography, {
    name: 'SupportCardText'
})({
    color: 'white',
    fontWeight: 'bold',
    position: 'absolute',
    top: '30%',
    right: '12%',
    left: '12%',
    textAlign: 'center',
})

export const SupportCardDescription = styled(Typography, {
    name: 'SupportCardDescription'
})({
    width: '100%'
})


export default SupportCardComponent;
