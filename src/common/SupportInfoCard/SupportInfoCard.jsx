import React from 'react';
import SupportCardComponent, { SupportCard, SupportCardSquare, SupportCardDescription, SupportCardNumber, SupportCardText } from './style';

const SupportInfoCard = ({ data }) => (
    <SupportCardComponent>
        <SupportCard>
            <SupportCardSquare>
                <SupportCardNumber variant='h4'>
                    {data.id}
                </SupportCardNumber>
                <SupportCardText variant='h3'>
                    {data.title}
                </SupportCardText>
            </SupportCardSquare>
            <SupportCardDescription>
                {data.description}
            </SupportCardDescription>
        </SupportCard>
    </SupportCardComponent>
);

export default SupportInfoCard;