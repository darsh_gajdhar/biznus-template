import { styled } from "@mui/system";
import { Box, Button, Container } from "@mui/material";

const HeaderBanner = styled(Box, {
    name: 'HeaderBanner'
})({
    position: 'sticky',
    top: '0px',
    zIndex: '800',
    boxSizing: 'border-box'
});

export const NavbarBanner = styled(Box, {
    name: 'NavBanner'
})({
    width: '100vw',
    backgroundColor: '#212121',
    padding: '17px 0px',
});

export const ContainerNavbar = styled(Container, {
    name: 'NavContainer'
})({
    display: 'flex',
    justifyContent: 'center',
    color: 'white',
    alignItems: 'center',
    fontSize: '18px',
    '@media (min-width: 0px) and (max-width:768px)': {
        flexDirection: 'column',
        fontSize: '15px'
    }
});

export const NavbarButton = styled(Button, {
    name: 'NavButton'
})({
    backgroundColor: '#eb5757',
    color: 'white',
    width: '145px',
    height: '35px',
    fontSize: '15px',
    '&:hover': { background: 'none' },
    textTransform: 'capitalize',
    borderRadius: '30px',
    marginRight: '20px',
    '@media (min-width: 0px) and (max-width:768px)': {
        marginBottom: '15px',
        width: '120px',
        height: '25px',
        fontSize: '12px'
    }
});

export default HeaderBanner;
