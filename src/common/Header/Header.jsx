import React from 'react';
import HeaderBanner, { NavbarBanner, NavbarButton, ContainerNavbar } from './style';
import Navbar from '../Navbar/Navbar';

const Header = () => (
  <HeaderBanner>
    <NavbarBanner>
      <ContainerNavbar >
        <NavbarButton>Announcement</NavbarButton>
        How we're responding to COVID-19
      </ContainerNavbar>
    </NavbarBanner>
    <Navbar />
  </HeaderBanner>
)

export default Header;