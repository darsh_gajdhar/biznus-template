import { styled } from "@mui/system";
import { Box, Typography } from "@mui/material";

const ProductsCard = styled(Box, {
    name: 'ProductsCard'
})({
    minHeight: '330px',
    borderRadius: '10px',
});

export const ProductImage = styled(Box, {
    name: 'ProductImage'
})({
    marginBottom: '10px',
    height: '440px',
    backgroundOrigin: 'border-box',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    'img': {
        borderRadius: '10px',
        width: '100%',
        height: '100%',
    },
    '@media (minWidth: 0px)': {
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
    }
});

export const ProductDetails = styled(Box, {
    name: 'ProductDetails'
})({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
});

export const ProductItem = styled(Typography, {
    name: 'ProductItem'
})({
    marginBottom: '3px',
    fontSize: '16px'
});
export const ProductPrice = styled(Typography, {
    name: 'ProductPrice'
})({
    fontWeight: 'bold',
    marginBottom: '8px',
    fontSize: '12px'
});

export default ProductsCard;