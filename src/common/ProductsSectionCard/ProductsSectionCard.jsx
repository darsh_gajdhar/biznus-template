import React from 'react'
import ProductsCard, { ProductDetails, ProductImage, ProductItem, ProductPrice } from './style';
import { ProductButton } from '../Button/style';

const ProductsSectionCard = (props) => (
    <ProductsCard>
        <ProductImage>
            <img src={props.data.image} alt={props.data.title} />
        </ProductImage>
        <ProductDetails>
            <ProductItem>
                {props.data.item}
            </ProductItem>
            <ProductPrice>
                {props.data.price}
            </ProductPrice>
            <ProductButton>Details</ProductButton>
        </ProductDetails>
    </ProductsCard>
)

export default ProductsSectionCard;