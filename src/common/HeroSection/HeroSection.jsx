import React from 'react';
import HeroSectionComponent, { HeroHeading, HeroWrapper } from './style';
import { Container } from '@mui/material';
import CustomButton from '../Button/Buttons'

const HeroSection = ({ image, heading, description }) => (
  <HeroSectionComponent
    sx={{ backgroundImage: `linear-gradient(180deg, rgba(34, 34, 34, 0.17), #222), url(${image})` }}
  >
    <Container>
      <HeroWrapper>
        <HeroHeading variant='h2'>
          {heading}
        </HeroHeading>
        <p>{description}
        </p>
        <CustomButton>Shop Merch</CustomButton>
      </HeroWrapper>
    </Container>
  </HeroSectionComponent>
)

export default HeroSection;