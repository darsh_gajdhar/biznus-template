import { Typography, Box } from "@mui/material";
import { styled } from "@mui/system";
import CustomButton from "../Button/Buttons";

const HeroSectionComponent = styled(Box, {
    name: 'HeroSectionComponent'
})({
    minHeight: '550px',
    backgroundSize: 'cover, auto',
    backgroundOrigin: 'border-box',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    '@media (min-width: 0px)': {
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
    }
});

export const HeroWrapper = styled(Box, {
    name: 'HeroWrapper'
})({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    color: 'white',
    minHeight: '550px',
    'p': {
        width: '35%',
        fontSize: '20px'
    },
    '@media (min-width: 0px) and (max-width:768px)': {

        'h2': {
            marginTop: '260px',
            width: '60%',
            fontSize: '30px'
        },
        'p': {

            width: '80%',
            fontSize: '16px'
        }
    }
});

export const HeroHeading = styled(Typography, {
    name: 'HeroHeading'
})({
    color: 'white',
    fontWeight: 'bold',
    maxWidth: '440px'
});

export const HeroButton = styled(CustomButton, {
    name: 'HeroButton'
})({
    fontWeight: 'bold',
    color: 'red',
    width: '130px',
    outline: '2px solid white',
    textTransform: 'capitalize',
    '&:hover': {
        backgroundColor: 'white',
        color: 'black'
    }
});

export default HeroSectionComponent;