import { Button } from "@mui/material";
import { styled } from "@mui/system";

const CustomButtonStyle = styled(Button, {
    name: 'CustomButtonStyle'
})({
    fontWeight: 'bold',
    color: 'white',
    outline: '2px solid white',
    textTransform: 'capitalize',
    '&:hover': {
        backgroundColor: 'white',
        color: 'black'
    }
});

export const ProductButton = styled(Button, {
    name: 'ProductButton'
})({

    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#eb5757',
    textTransform: 'capitalize',
    '&:hover': {
        backgroundColor: 'black',
        color: 'white'
    }
});

export const ContactButton = styled(Button, {
    name: 'ContactButton'
})({

    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#eb5757',
    textTransform: 'capitalize',
    '&:hover': {
        backgroundColor: 'black',
        color: 'white'
    }
});

export default CustomButtonStyle;