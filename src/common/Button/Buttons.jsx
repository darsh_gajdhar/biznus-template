import React from 'react';
import CustomButtonStyle from './style'

const CustomButton = ({ children, ...rest }) => (
  <CustomButtonStyle {...rest}> {children}</CustomButtonStyle>
)

export default CustomButton;