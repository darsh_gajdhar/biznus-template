import React from 'react';
import { Container } from '@mui/system';
import FooterComponent, { FooterWrapper, FooterLogo, FooterSocialLinks, FooterEndLine } from './style';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookSharpIcon from '@mui/icons-material/FacebookSharp';
import TwitterIcon from '@mui/icons-material/Twitter';
import { Link } from 'react-router-dom';

const Footer = () => (
  <FooterComponent>
    <Container>
      <FooterWrapper>
        <FooterLogo>
          <Link to='/'>
            <img src='https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e7ff57adad44d1f072965b6_logo.svg' alt='Acme OutDoors Logo' />
          </Link>
        </FooterLogo>
        <FooterSocialLinks>
          <a href="https://twitter.com/webflow" target='_blank' rel="noreferrer">
            <TwitterIcon />
          </a>
          <a href="https://www.facebook.com/webflow" target='_blank' rel="noreferrer">
            <FacebookSharpIcon />
          </a>
          <a href="https://www.instagram.com/webflow/" target='_blank' rel="noreferrer">
            <InstagramIcon />
          </a>
        </FooterSocialLinks>
      </FooterWrapper>
      <FooterEndLine>
        Made In<span>Webflow</span>© 2020.
      </FooterEndLine>
    </Container>
  </FooterComponent>
)

export default Footer;