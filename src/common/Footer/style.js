import { Box } from "@mui/material";
import { styled } from "@mui/system";

const FooterComponent = styled(Box, {
    name: 'FooterComponent',
})({
    backgroundColor: '#212121',
    height: '120px',
    paddingTop: '50px',
    '@media (min-width:0px) and (max-width:768px)': {
        height: 'auto'
    },
});

export const FooterWrapper = styled(Box, {
    name: 'FooterWrapper'
})({
    display: 'flex',
    justifyContent: 'space-between',
    color: 'white',
    '@media (min-width:0px) and (max-width:768px)': {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export const FooterLogo = styled(Box, {
    name: 'FooterLogo'
})({
    display: 'flex',
    width: '50%',
    marginBottom: '10px',
    '@media (min-width:0px) and (max-width:768px)': {
        justifyContent: 'center',
    }
});

export const FooterSocialLinks = styled(Box, {
    name: 'FooterSocialLinks'
})({
    width: '50%',
    display: 'flex',
    justifyContent: 'flex-end',
    'a': {
        marginLeft: '20px',
        color: 'white',
    },
    '@media (min-width:0px) and (max-width:768px)': {
        justifyContent: 'space-between',
        alignItems: 'center',
    }
});

export const FooterEndLine = styled(Box, {
    name: 'FooterEndLine'
})({
    width: '100%',
    marginTop: '40px',
    color: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    wordSpacing: '4px',
    fontSize: '10px',
    'span': {
        wordSpacing: '10px',
        margin: '0 5px',
        cursor: 'pointer',
        textDecoration: 'underline',
        color: 'red'
    },
    '@media (min-width: 0px) and (max-width:768px)': {
        marginTop: '30px',
        paddingBottom: '20px'
    }
})

export default FooterComponent;