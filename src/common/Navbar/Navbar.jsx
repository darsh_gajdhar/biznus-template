import React from "react";
import NavBarComponent, { ContainerNavbar, NavBarDiv } from './style';
import { useMediaQuery, useTheme } from '@mui/material';
import { Link } from 'react-router-dom';
import Cart from '../../assets/images/cart.svg';
import DrawerContainer from '../Drawer/DrawerContainer';

const Navbar = () => {
  const theme = useTheme();
  const isMatch = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <NavBarComponent>
      <ContainerNavbar>
        <Link to='/'>
          <img src='https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e7ff57adad44d1f072965b6_logo.svg' alt='Acme OutDoors Logo' />
        </Link>
        <NavBarDiv>
          {isMatch ? (
            <>
              <Link to='/'><img src={Cart} alt='cart-img' /></Link>
              <DrawerContainer />
            </>
          ) : (
            <>
              <Link to='/home'>Home</Link>
              <Link to='/about'>About</Link>
              <Link to='/shop'>Shop</Link>
              <Link to='/donate'>Donate</Link>
              <Link to='contact'>Contact</Link>
              <Link to='/'><img src={Cart} alt='cart-img' /></Link>
            </>
          )}
        </NavBarDiv>
      </ContainerNavbar>
    </NavBarComponent>
  );
};

export default Navbar;