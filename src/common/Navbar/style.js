import { styled } from "@mui/system";
import { Box, Container } from "@mui/material";

const NavBarComponent = styled(Box, {
    name: 'NavBarComponent'
})({
    width: '100vw',
    backgroundColor: '#eb5757',
    padding: '17px 0px',
    fontSize: '15px',
    boxSizing: 'border-box'
});

export const ContainerNavbar = styled(Container, {
    name: 'ContainerNavbar'
})({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
});

export const NavBarDiv = styled(Container, {
    name: 'NavBarDiv'
})({
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    a: {
        padding: '7px 25px',
        marginLeft: '20px',
        color: 'white',
        textDecoration: 'none'
    },
    'a:hover': {
        backgroundColor: 'black',
        borderRadius: '5px',
    },
    img: {
        width: '30px',
        height: '25px',
        backgroundColor: 'black',
        borderRadius: '5px',
    },
    '@media (min-width: 0px) and (max-width:768px)': {
        a: {
            padding: '7px 15px',
            marginLeft: '10px',
            color: 'white',
            textDecoration: 'none'
        },
        'a:hover': {
            backgroundColor: 'black',
            borderRadius: '5px',
            padding: '7px'
        },
    }
});

export default NavBarComponent;