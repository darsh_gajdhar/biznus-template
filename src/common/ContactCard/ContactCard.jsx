import React from 'react'
import { ContactButton } from '../Button/style';
import ContactCardComponent, { ContactCardDescription, ContactCardHeading } from './style';

const ContactCard = ({ data }) => (
    <ContactCardComponent>
        <img src={data.image} alt={data.title} />
        <ContactCardHeading>
            {data.title}
        </ContactCardHeading>
        <ContactCardDescription>
            {data.description}
        </ContactCardDescription>
        <ContactButton>{data.buttonLabel}</ContactButton>
    </ContactCardComponent>
)

export default ContactCard;