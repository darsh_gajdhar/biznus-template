import { styled, Typography } from '@mui/material';
import { Box } from '@mui/material';

const ContactCardComponent = styled(Box, {
    name: 'ContactCardComponent'
})({
    borderRadius: '5px',
    minHeight: 'auto',
    backgroundColor: 'white',
    padding: '35px',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    'img': {
        marginBottom: '25px'
    }
});

export const ContactCardHeading = styled(Typography, {
    name: 'ContactCardHeading'
})({
    fontWeight: '600',
    margin: '15px 0',
    fontSize: '30px'
})

export const ContactCardDescription = styled(Typography, {
    name: 'ContactCardDescription'
})({
    fontSize: '16px',
    width: '90%',
    textAlign: 'center',
    marginBottom: '15px'
})

export default ContactCardComponent;