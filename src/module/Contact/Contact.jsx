import React from 'react'
import ContactSection from '../ContactSection/ContactSection'

const Contact = () => (
    <ContactSection />
)

export default Contact