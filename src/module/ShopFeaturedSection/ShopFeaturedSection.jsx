import { Container } from '@mui/material'
import React from 'react'
import CustomButton from '../../common/Button/Buttons'
import ShopFeaturedComponent, { ShopFeaturedItem, ShopFeaturedItemContent, ShopFeaturedItemPill, ShopFeaturedProductName, ShopFeaturedProductPrice } from './style'

const ShopFeaturedSection = () => (
    <ShopFeaturedComponent>
        <Container>
            <ShopFeaturedItem>
                <ShopFeaturedItemPill>
                    <CustomButton>Featured Item</CustomButton>
                </ShopFeaturedItemPill>
                <ShopFeaturedItemContent>
                    <ShopFeaturedProductName variant='h5'>White Tent</ShopFeaturedProductName>
                    <ShopFeaturedProductPrice>
                        $ 200.00 USD
                    </ShopFeaturedProductPrice>
                </ShopFeaturedItemContent>
            </ShopFeaturedItem>
        </Container>
    </ShopFeaturedComponent>
)

export default ShopFeaturedSection