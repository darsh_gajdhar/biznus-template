import { Box, Typography } from "@mui/material";
import { styled } from "@mui/system";

const ShopFeaturedComponent = styled(Box, {
    name: 'ShopFeaturedComponent'
})({
    paddingTop: '50px'
});

export const ShopFeaturedItem = styled(Box, {
    name: 'ShopFeaturedItem'
})({
    backgroundImage: 'linear-gradient(30deg, rgba(14, 14, 14, 0.17), #222), url("https://assets.website-files.com/5e853c3383474026e43f2c78/5e856e41c718420c18dd6751_patrick-hendry-eDgUyGu93Yw-unsplash.jpg")',
    minHeight: '220px',
    borderRadius: '5px',
    marginBottom: '20px',
    backgroundSize: 'cover',
    backgroundPosition: '50% 50%',
    position: 'relative',
});

export const ShopFeaturedItemPill = styled(Box, {
    name: 'ShopFeaturedItemPill'
})({
    borderRadius: '5px',
    color: '#212121',
    position: 'absolute',
    top: '5%',
    right: '2%'
});

export const ShopFeaturedItemContent = styled(Box, {
    name: 'ShopFeaturedItemContent'
})({
    position: 'absolute',
    bottom: '10%',
    left: '2%',
    color: 'white',
    display: 'flex',
    flexDirection: 'column',
});

export const ShopFeaturedProductName = styled(Typography, {
    name: 'ShopFeaturedProductName'
})({
    fontWeight: 'bold',
    marginBottom: '8px'
});

export const ShopFeaturedProductPrice = styled(Typography, {
    name: 'ShopFeaturedProductPrice'
})({
    fontSize: '14px'
})

export default ShopFeaturedComponent;