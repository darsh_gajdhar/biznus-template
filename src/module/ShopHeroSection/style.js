import { Box, Typography } from "@mui/material";
import { styled } from "@mui/system";

const ShopHeroComponent = styled(Box, {
    name: 'ShopHeroSection'
})({
    backgroundColor: '#212121',
    color: 'white',
    padding: '30px 0px',
});

export const ShopHeroHeading = styled(Typography, {
    name: 'ShopHeroHeading'
})({
    fontWeight: 'bold',
    '@media (min-width: 0px) and (max-width: 768px)': {
        width: '50%'
    }
})

export default ShopHeroComponent; 