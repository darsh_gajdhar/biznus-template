import { Container } from '@mui/material';
import React from 'react'
import ShopHeroComponent, { ShopHeroHeading } from './style';

const ShopHeroSection = () => (
    <ShopHeroComponent>
        <Container>
            <ShopHeroHeading variant='h4'>
                Shop Our Products
            </ShopHeroHeading>
        </Container>
    </ShopHeroComponent>
)

export default ShopHeroSection;