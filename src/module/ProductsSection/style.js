import { styled } from "@mui/system";
import { Box, Typography } from '@mui/material'

const ProductsSectionComponent = styled(Box, {
    name: 'ProductsSectionComponent'
})({
    padding: '40px 0px',
});

export const ProductsTopWrapper = styled(Box, {
    name: 'ProductsTopWrapper'
})({
    marginBottom: '20px'
});

export const ProductsTopContentHeading = styled(Typography, {
    name: 'ProductsTopContentHeading'
})({
    textTransform: 'uppercase',
    fontSize: '14px'
});

export const ProductsTopContentDetails = styled(Typography, {
    name: 'ProductsTopContentDetails'
})({
    fontWeight: 'bold',
});

export const ProductsListContainer = styled(Box, {
    name: 'ProductsListContainer'
})({
    marginBottom: '50px',
});

export const ProductsBottomWrapper = styled(Box, {
    name: 'ProductsBottomWrapper'
})({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
})

export default ProductsSectionComponent;