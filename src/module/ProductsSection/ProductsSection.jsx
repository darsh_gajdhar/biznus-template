import { Container } from '@mui/material';
import React from 'react'
import ProductsSectionCard from '../../common/ProductsSectionCard/ProductsSectionCard';
import ProductsSectionComponent, { ProductsTopContentDetails, ProductsTopContentHeading, ProductsTopWrapper, ProductsListContainer, ProductsBottomWrapper } from './style';
import ProductsInfo from "../../utils/JSON/productsSection.json";
import { Grid } from '@mui/material';
import { ProductButton } from '../../common/Button/style';

const ProductsSection = () =>
(
    <ProductsSectionComponent>
        <Container>
            <ProductsTopWrapper>
                <ProductsTopContentHeading>
                    Shop products
                </ProductsTopContentHeading>
                <ProductsTopContentDetails variant='h4'>
                    Open 24/7/365.
                </ProductsTopContentDetails>
            </ProductsTopWrapper>
            <ProductsListContainer>
                <Grid container rowSpacing={4} columnSpacing={4}>
                    {ProductsInfo.map((productsInfo) => {
                        return (
                            <Grid item key={productsInfo.id} lg={4} xs={12} >
                                <ProductsSectionCard data={productsInfo} />
                            </Grid>
                        )
                    })}
                </Grid>
            </ProductsListContainer>
            <ProductsBottomWrapper>
                <ProductButton>View All Products</ProductButton>
            </ProductsBottomWrapper>
        </Container>
    </ProductsSectionComponent>
)

export default ProductsSection;