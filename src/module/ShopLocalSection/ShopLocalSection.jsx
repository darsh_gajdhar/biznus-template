import React from 'react'
import ShopLocalSectionComponent, { ContainerShopLocal, ShopLocalSectionWrapper, ShopLocalWrapperContent, ShopLocalWrapperContentEnd, ShopLocalWrapperHeading, ShopLocalWrapperLeft, ShopLocalWrapperRight, ShopLocalWrapperRightContent } from './style';

const ShopLocalSection = () => (
    <ShopLocalSectionComponent>
        <ContainerShopLocal>
            <ShopLocalSectionWrapper>
                <ShopLocalWrapperLeft />
                <ShopLocalWrapperRight>
                    <ShopLocalWrapperRightContent>
                        <ShopLocalWrapperHeading variant='h3'>
                            Shop Local.
                        </ShopLocalWrapperHeading>
                        <ShopLocalWrapperContent>
                            We know that during COVID-19, a lot of folks around the city and state are feeling uneasy about the future - we're not sure what the future holds either.
                        </ShopLocalWrapperContent>
                        <ShopLocalWrapperContent>
                            That said: we know that we love making sure you have the gear you need for your adventures, and we're going to keep doing that - with our team - until the city tells us we can't.
                        </ShopLocalWrapperContent>
                        <ShopLocalWrapperContent>
                            But as long as folks like yourself support small businesses around the city, then we'll be here — every day, making sure your orders arrive on time.
                        </ShopLocalWrapperContent>
                        <ShopLocalWrapperContent>
                            -------
                        </ShopLocalWrapperContent>
                        <ShopLocalWrapperContentEnd>
                            Jane & John Doe
                        </ShopLocalWrapperContentEnd>
                        <ShopLocalWrapperContentEnd fontWeight='bold'>
                            Acme Outdoors
                        </ShopLocalWrapperContentEnd>
                    </ShopLocalWrapperRightContent>
                </ShopLocalWrapperRight>
            </ShopLocalSectionWrapper>
        </ContainerShopLocal>
    </ShopLocalSectionComponent>
)

export default ShopLocalSection;