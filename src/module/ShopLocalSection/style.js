import { styled } from "@mui/system";
import { Box, Card, Typography, Container } from "@mui/material";

const ShopLocalSectionComponent = styled(Box, {
    name: 'ShopLocalSectionComponent'
})({
    padding: '60px 0px',
    boxSizing: 'border-box',
    '@media (max-width: 768px) and (min-width: 0px)': {
        padding: '0'
    }
});

export const ContainerShopLocal = styled(Container, {
    name: 'ContainerShopLocal'
})({
    '@media (max-width: 768px) and (min-width: 0px)': {
        padding: '0'
    }
})

export const ShopLocalSectionWrapper = styled(Box, {
    name: 'ShopLocalSectionWrapper'
})({
    display: 'flex',
    position: 'relative',
    zIndex: 'auto',
    '@media (min-width: 0px) and (max-width:768px)': {
        flexDirection: 'column',
        position: 'static'
    }
});

export const ShopLocalWrapperLeft = styled(Card, {
    name: 'ShopLocalWrapperLeft'
})({
    marginTop: '30px',
    width: '33%',
    height: '440px',
    backgroundImage: 'url("https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e83fe3910db4fde2e69f396_christiann-koepke-dQyS2pMYtok-unsplash%20(1).jpg")',
    backgroundPosition: '69% 50%',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '290%',
    'img': {
        borderRadius: '10px',
        width: '100%',
        height: '100%',
    },
    position: 'absolute',
    zIndex: '3',
    '@media (min-width: 0px) and (max-width:768px)': {
        backgroundSize: 'auto, cover',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        margin: '0',
        padding: '0',
        position: 'static',
        borderRadius: '0px'
    },
    transition: 'none'
});

export const ShopLocalWrapperRight = styled(Box, {
    name: 'ShopLocalWrapperRight'
})({

    backgroundColor: '#212121',
    zIndex: '1',
    height: 'auto',
    width: '80%',
    marginLeft: '20%',
    borderRadius: '5px',
    display: 'flex',
    padding: '40px 60px',
    color: 'white',
    '@media (min-width:0px) and (max-width:768px)': {
        position: 'static',
        marginLeft: '0',
        borderRadius: '0px',
        width: 'auto',
        padding: '40px 30px'
    }
});

export const ShopLocalWrapperRightContent = styled(Box, {
    name: 'ShopLocalWrapperRightContent'
})({
    width: '80%',
    marginLeft: '70px',
    padding: '0px 180px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    textAlign: 'left',
    '@media (min-width: 0px) and (max-width:768px)': {
        marginLeft: '0px',
        width: '100%',
        padding: '0px',
    }
});

export const ShopLocalWrapperHeading = styled(Typography, {
    name: 'ShopLocalWrapperHeading'
})({
    lineHeight: '1.2',
    marginBottom: '15px',
    fontWeight: 'bold',
});

export const ShopLocalWrapperContent = styled(Typography, {
    name: 'ShopLocalWrapperContent'
})({
    lineHeight: '1.5',
    marginBottom: '13px',
    align: 'left',
});

export const ShopLocalWrapperContentEnd = styled(Typography, {
    name: 'ShopLocalWrapperContentEnd'
})({
    lineHeight: '1.4',
});

export default ShopLocalSectionComponent;