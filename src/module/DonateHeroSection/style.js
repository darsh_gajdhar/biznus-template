import { styled } from "@mui/system";
import { Box, Container, Typography } from "@mui/material";

const DonateHeroSectionComponent = styled(Box, {
    name: 'DonateHeroSectionComponent'
})({
    minHeight: '550px',
    backgroundImage: 'linear-gradient(180deg, rgba(34, 34, 34, 0.17), #222), url("https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e85758a28ae07433140f0cb_jed-villejo-bEcC0nyIp2g-unsplash%20(1)%20(1).jpg")',
    backgroundSize: ' auto',
    backgroundOrigin: 'border-box',
    backgroundPosition: '0px 0px, 50% 25%',
    backgroundRepeat: 'no-repeat',
    paddingTop: '140px',
    '@media (min-width: 0px)': {
        backgroundSize: 'cover',
        backgroundPosition: '0px 0px, 40% 25%',
        backgroundRepeat: 'no-repeat'
    }
})

export const ContainerDonateHeroSection = styled(Container, {
    name: 'ContainerDonateHeroSection'
})({
    marginTop: '30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
})

export const DonateHeroSectionMainHeading = styled(Typography, {
    name: 'DonateHeroSectionHeading'
})({
    fontWeight: '750',
    color: 'white',
    '@media (min-width:0px) and (max-width:768px)': {
        width: '70%',
        fontSize: '28px',
        textAlign: 'center'
    }
});

export const DonateHeroSectionHeading = styled(Typography, {
    name: 'DonateHeroSectionHeading'
})({
    color: 'white',
    textAlign: 'center',
    fontSize: '30px',
    marginBottom: '15px',
    '@media (min-width: 0px) and (max-width:768px)': {
        width: '70%',
        fontSize: '25px'
    }
})

export const DonateHeroSectionDescription = styled(Typography, {
    name: 'DonateHeroSectionDescription'
})({
    color: 'white',
    textAlign: 'center',
    width: '65%',
    '@media (min-width: 0px) and (max-width:768px)': {
        fontSize: '14px',
        width: '80%',
        texAlign: 'none'
    }
});

export default DonateHeroSectionComponent;