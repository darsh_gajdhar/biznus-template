import React from 'react'
import DonateHeroSectionComponent, { DonateHeroSectionMainHeading, DonateHeroSectionHeading, ContainerDonateHeroSection, DonateHeroSectionDescription } from './style';

const DonateHeroSection = () => (
    <DonateHeroSectionComponent>
        <ContainerDonateHeroSection>
            <DonateHeroSectionMainHeading variant='h4'>
                Here at Acme Outdoors
            </DonateHeroSectionMainHeading>
            <DonateHeroSectionHeading>
                every dollar counts
            </DonateHeroSectionHeading>
            <DonateHeroSectionDescription>
                Acme Outdoors is more than just a company, we're a community of people who care for one another and for our city. During this time, due to shelter in place orders, only a select few of our staff are able to work. Any donations you make to Acme will help make sure our employees are cared for and can stay safe in these uncertain times.
            </DonateHeroSectionDescription>
        </ContainerDonateHeroSection>
    </DonateHeroSectionComponent>
)

export default DonateHeroSection;