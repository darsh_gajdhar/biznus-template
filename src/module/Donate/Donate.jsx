import React from 'react'
import DonateHeroSection from '../DonateHeroSection/DonateHeroSection'

const Donate = () => (
    <DonateHeroSection />
)

export default Donate