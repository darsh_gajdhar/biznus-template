import React from 'react'
import ContactSectionComponent, { ContactSectionImage, ContactSectionHeading, ContactSectionWrapper } from './style';
import ContactDetails from '../../utils/JSON/contactCard.json';
import ContactCard from '../../common/ContactCard/ContactCard';
import { Grid } from '@mui/material';

const ContactSection = () => (
    <ContactSectionComponent>
        <ContactSectionImage />
        <ContactSectionWrapper>
            <ContactSectionHeading>
                Contact Acme Outdoors
            </ContactSectionHeading>
            <Grid container rowSpacing={3} columnSpacing={3}>
                {ContactDetails.map((contactDetails) => {
                    return (
                        <Grid item key={contactDetails.id} lg={4} xs={12}>
                            <ContactCard data={contactDetails} />
                        </Grid>
                    )
                })}
            </Grid>
        </ContactSectionWrapper>
    </ContactSectionComponent>
)

export default ContactSection;