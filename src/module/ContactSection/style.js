import { Box, Container, Typography } from "@mui/material";
import { styled } from "@mui/system";

const ContactSectionComponent = styled(Box, {
    name: 'ContactSectionComponent'
})({
    boxSizing: 'border-box',
    display: 'flex',
    justifyContent: 'flex-end',
    position: 'relative',
    backgroundColor: '#212121',
    '@media (min-width: 0px) and (max-width: 768px)': {
        flexDirection: 'column',
    }
});

export const ContactSectionWrapper = styled(Container, {
    name: 'ContactSectionWrapper'
})({
    position: 'absolute',
    right: '0',
    left: '0',
    bottom: '0',
    top: '0',
    '@media(max-width:768px) and (min-width:0px)': {
        position: 'static'
    }
})

export const ContactSectionHeading = styled(Typography, {
    name: 'ContactSectionHeading'
})({
    marginTop: '40px',
    marginBottom: '10px',
    color: 'white',
    fontWeight: '600',
    padding: '0px, 50px',
    fontSize: '35px',
    wordSpacing: '3px'
})

export const ContactSectionImage = styled(Box, {
    name: 'ContactSectionImage'
})({
    minHeight: '550px',
    backgroundImage: 'linear-gradient(90deg, #222, rgba(34, 34, 34, 0.18) 76%), url("https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e84f1e1f02ef1408ff221cc_vinicius-amano-4mK2KVuYrDs-unsplash%20(1)%20(1).jpg")',
    backgroundSize: 'cover',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    width: '45%',
    marginLeft: 'auto',
    '@media (min-width: 0px) and (max-width:768px)': {
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        margin: '0',
        width: '100%',
        minHeight: '400px'
    },
})

export default ContactSectionComponent;