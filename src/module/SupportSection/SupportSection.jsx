import { Container, Grid, Typography } from '@mui/material';
import React from 'react';
import SupportInfoCard from '../../common/SupportInfoCard/SupportInfoCard';
import SupportSectionComponent, {
  SupportSectionTopLeftDiv, SupportSectionTopWrapper,
  SupportSectionTopRightDiv, SupportSectionTopLeftHeading,
} from './style';
import SupportCardInfo from "../../utils/JSON/supportSection.json";

const SupportSection = () => (
  <SupportSectionComponent>
    <Container>
      <SupportSectionTopWrapper>
        <SupportSectionTopLeftDiv>
          <Typography>WAYS TO SUPPORT</Typography>
          <SupportSectionTopLeftHeading variant='h4'>Support Acme Outdoors.</SupportSectionTopLeftHeading>
        </SupportSectionTopLeftDiv>
        <SupportSectionTopRightDiv>COVID-19 has forced us to close our retail space, but we need support from patrons like yourself now more than ever. Below, we've listed the best ways to help us through this season.</SupportSectionTopRightDiv>
      </SupportSectionTopWrapper>
      <Grid container spacing={8}>
        {SupportCardInfo.map((dataInfo) => {
          return (
            <Grid item key={dataInfo.id} xs={12} lg={4}>
              <SupportInfoCard data={dataInfo} />
            </Grid>
          );
        })}
      </Grid>
    </Container>
  </SupportSectionComponent>
)

export default SupportSection;