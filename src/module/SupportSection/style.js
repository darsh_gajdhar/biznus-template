import { styled } from "@mui/system";
import { Typography, Box } from "@mui/material";

const SupportSectionComponent = styled(Box, {
    name: 'SupportSectionComponent'
})({
    height: 'auto',
    padding: '60px 0px',
    boxSizing: 'border-box'
});

export const SupportSectionTopWrapper = styled(Box, {
    name: 'SupportSectionTopWrapper'
})({
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '40px',
    '@media (min-width: 0px) and (max-width:768px)': {
        flexDirection: 'column'
    }
});

export const SupportSectionTopLeftDiv = styled(Box, {
    name: 'SupportSectionTopLeftDiv'
})({
    display: 'flex',
    flexDirection: 'column',
    width: '50%',
    '@media (min-width: 0px) and (max-width:768px)': {
        width: '100%',
        flexDirection: 'column',
    }
});

export const SupportSectionTopLeftHeading = styled(Typography, {
    name: 'SupportSectionTopLeftHeading'
})({
    fontWeight: 'bold'
})

export const SupportSectionTopRightDiv = styled(Typography, {
    name: 'SupportSectionTopRightDiv'
})({
    width: '45%',
    fontSize: '16.5px',
    '@media (min-width: 0px) and (max-width:768px)': {
        width: '100%',
    }
});

export default SupportSectionComponent;