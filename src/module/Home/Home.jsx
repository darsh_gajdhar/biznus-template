import React from 'react';
import HeroSection from '../../common/HeroSection/HeroSection';
import SupportSection from '../SupportSection/SupportSection';
import SafeSection from '../SafeSection/SafeSection';
import ProductsSection from '../ProductsSection/ProductsSection';
import ShopLocalSection from '../ShopLocalSection/ShopLocalSection';

const Home = () => (
    <>
        <HeroSection
            image='https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e83f98bae1ad80129fcd7e5_wu-jianxiong-UniC8xhlzaE-unsplash.jpg'
            heading='Serving you since 1989.'
            description="Acme Outdoors is an outdoor and adventure shop located in the Boathouse District in Oklahoma City."
        />
        <SupportSection />
        <SafeSection />
        <ProductsSection />
        <ShopLocalSection />
    </>
)

export default Home;