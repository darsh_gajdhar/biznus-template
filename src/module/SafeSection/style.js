import { styled } from "@mui/system";
import { Box, Typography } from "@mui/material";

const SafeSectionComponent = styled(Box, {
    name: 'SafeSectionComponent'
})({
    minHeight: '550px',
    backgroundImage: 'linear-gradient(180deg, rgba(34, 34, 34, 0.17), #222), url(https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e83fb3a6948e1f05b20527f_nathan-dumlao-pLoMDKtl-JY-unsplash.jpg)',
    backgroundSize: 'cover, auto',
    backgroundOrigin: 'border-box',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    '@media (min-width: 0px)': {
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
    },
    position: 'relative'
});

export const SafeSectionWrapper = styled(Box, {
    name: 'SafeSectionWrapper'
})({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    color: 'white',
    position: 'absolute',
    right: '0',
    top: '30%',
    '@media (min-width:0px) and (max-width:768px)': {
        top: '40%'
    }
});

export const SafeSectionContents = styled(Box, {
    name: 'SafeSectionContents'
})({
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'column',
    width: '60%',
    textAlign: 'right',
    '@media (min-width: 0px) and (max-width:768px)': {
        width: '90%',

    }
})

export const SafeSectionContentHeading = styled(Typography, {
    name: 'SafeSectionContentHeading'
})({
    fontWeight: 'bold',
    width: '85%',
    fontSize: '40px',
    marginBottom: '10px',
    '@media (min-width: 0px) and (max-width: 768px)': {
        fontSize: '28px'
    }

});

export const SafeSectionContentParagraph = styled(Typography, {
    name: 'SafeSectionContentParagraph'
})({
    width: '70%',
    fontSize: '18px',
    textAlign: 'right',
    lineHeight: '1.7',
    marginBottom: '20px',
    '@media (min-width: 0px) and (max-width:768px)': {
        fontSize: '15px',
    }
})

export default SafeSectionComponent;