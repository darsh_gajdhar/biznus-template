import { Container } from '@mui/system';
import React from 'react';
import CustomButton from '../../common/Button/Buttons';
import SafeSectionComponent, { SafeSectionContents, SafeSectionWrapper, SafeSectionContentHeading, SafeSectionContentParagraph } from './style';

const SafeSection = () => (
    <SafeSectionComponent>
        <Container>
            <SafeSectionWrapper>
                <SafeSectionContents>
                    <SafeSectionContentHeading>
                        How we're keeping you safe during COVID-19
                    </SafeSectionContentHeading>
                    <SafeSectionContentParagraph>
                        As an outdoor shop, we've taken precautionary measures to ensure the safety of all our customers and team members.
                    </SafeSectionContentParagraph>
                    <CustomButton>Read Our Statement</CustomButton>
                </SafeSectionContents>
            </SafeSectionWrapper>
        </Container>
    </SafeSectionComponent>
)

export default SafeSection;