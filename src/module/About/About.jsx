import React from 'react';
import HeroSection from '../../common/HeroSection/HeroSection';


const About = () => (
    <HeroSection
        image='https://assets.website-files.com/5e7ff3ec0c4ef4c974fa99e3/5e840df8e2c9cc7677e4f00e_felix-rostig-UmV2wr-Vbq8-unsplash.jpg'
        heading='Your Adventure
                Awaits'
        description='Acme Outdoors has everything you need to help you get started today. Check out our wonderful collection of gear that will make your next adventure complete.'
    />
)

export default About;