import React from 'react'
import ShopFeaturedSection from '../ShopFeaturedSection/ShopFeaturedSection'
import ShopHeroSection from '../ShopHeroSection/ShopHeroSection'

const Shop = () => (
    <>
        <ShopHeroSection />
        <ShopFeaturedSection />
    </>
)

export default Shop